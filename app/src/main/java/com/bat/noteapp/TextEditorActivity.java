package com.bat.noteapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class TextEditorActivity extends AppCompatActivity implements TextWatcher {

    private EditText mEditText;

    private int noteId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_editor);

        this.setTitle("Edit Your Node");

        mEditText = (EditText) findViewById(R.id.editText);

        Intent intent = getIntent();
        noteId = intent.getIntExtra("note_id", -1);

        if(noteId != -1){
            mEditText.setText(MainActivity.noteList.get(noteId));
        }

        mEditText.addTextChangedListener(this);

    }

//    @Override
//    protected void onStop() {
//
//        String savedText = mEditText.getText().toString();
//        if(savedText.length() > 0) {
////            getSharedPreferences("com.bat.noteapp", Context.MODE_PRIVATE)
////                    .edit()
////                    .putString("TEXTNOTE", mEditText.getText().toString());
//            MainActivity.noteList.add(savedText);
//            MainActivity.arrayAdapter.notifyDataSetChanged();
//        }
//        super.onStop();
//    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if(noteId == -1) {
            MainActivity.noteList.add(0,s.toString());
            noteId = 0;
        }else {
            MainActivity.noteList.set(noteId, s.toString());

        }
            MainActivity.noteSet.clear();
            MainActivity.noteSet.addAll(MainActivity.noteList);
            getSharedPreferences("com.bat.noteapp", Context.MODE_PRIVATE).edit().remove("NOTES");
            getSharedPreferences("com.bat.noteapp", Context.MODE_PRIVATE).edit().putStringSet("NOTES", MainActivity.noteSet).apply();



    }

    @Override
    public void afterTextChanged(Editable s) {


    }

    @Override
    protected void onPause() {
        MainActivity.arrayAdapter.notifyDataSetChanged();
        super.onPause();
    }
}
