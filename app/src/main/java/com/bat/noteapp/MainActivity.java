package com.bat.noteapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    public static ArrayList<String> noteList;
    public static ArrayAdapter<String> arrayAdapter;

    public static Set<String> noteSet;

    private SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = this.getSharedPreferences("com.bat.noteapp", Context.MODE_PRIVATE);
        sharedPreferences.edit().apply();


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), TextEditorActivity.class);
                intent.putExtra("note_id", -1);
                startActivity(intent);
            }
        });

        final ListView listView = (ListView) findViewById(R.id.listView);

        noteSet = sharedPreferences.getStringSet("NOTES", null);

        noteList = new ArrayList<>();

        final ArrayList<String> noteTitles = new ArrayList<>();

        if(noteSet != null){

            noteList.addAll(noteSet);

            for(int i=0; i < noteList.size(); i++){
                String title = noteList.get(i);
                if(title.length() > 30){
                    title = title.substring(0, 30) + "...";
                }
                noteTitles.add(title);
            }

        }else{
            noteSet = new HashSet<>();
            //noteSet.addAll(noteList);
            sharedPreferences.edit().remove("NOTES");
            sharedPreferences.edit().putStringSet("NOTES", noteSet).apply();
        }

        Log.i("OPALA", "CREATED");
        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, noteTitles);

        listView.setAdapter(arrayAdapter);
        arrayAdapter.notifyDataSetChanged();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), TextEditorActivity.class);
                intent.putExtra("note_id", position);
                startActivity(intent);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

                new AlertDialog.Builder(MainActivity.this)
                        .setIcon(android.R.drawable.ic_delete)
                        .setTitle("Are you Sure?")
                        .setMessage("Do you want to delete this note?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                noteList.remove(position);
                                noteTitles.remove(position);

                                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("com.bat.noteapp", Context.MODE_PRIVATE);

                                if(noteSet == null){
                                    noteSet = new HashSet<String>();
                                }else{
                                    noteSet.clear();
                                }

                                noteSet.addAll(noteList);
                                sharedPreferences.edit().remove("NOTES");
                                sharedPreferences.edit().putStringSet("NOTES", noteSet).apply();
                                arrayAdapter.notifyDataSetChanged();


                            }
                        })
                        .setNegativeButton("No", null)
                        .show();

                arrayAdapter.notifyDataSetChanged();
                return true ;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    protected void onDestroy() {
        sharedPreferences.edit().remove("NOTES");
        sharedPreferences
                .edit()
                .putStringSet("NOTES", noteSet)
                .apply();
        Log.i("NOTEAPP", "Bye Bye");
        super.onDestroy();
    }
}
